# DSE 250 HW1

## Javier Garcia

### I. 

`Person (ssn VARCHAR(9) PRIMARY KEY, name text, dob Date)`

`Boat (boatid int PRIMARY KEY, name text, tonnage int)`

`RacesWon (raceid int PRIMARY KEY, race_name text, boatfk int REFERENCES Boat(boatid))`

`Ownership (ownid int PRIMARY KEY, begin_date date, end_date date, boatfk int REFERENCES Boat(boatid))`

`CoOwners (coownid int PRIMARY KEY, ownid int REFERENCES Ownership(ownid), ssn VARCHAR(9) REFERENCES Person(ssn))`


Caveats: inverse relationship, extent keyword, sets aren’t native to SQL. These have to be manually maintained with SQL.
In practice, this means that I need extra tables to express the same without object-oriented features. I needed to add the co-owner and races won table to preserve the same expression level. 

### II
#### OQL

1. 
```
SELECT DISTINCT struct ( boat: b,  owner: p )
FROM b in boats, p in b.belongedTo.coOwners
WHERE ‘Americas Cup’ in b.racesWon
```

2.
```
SELECT DISTINCT o.boat
FROM o in ownerships
WHERE exists p in o.coOwners: p.name = "Jack Sparrow"
```

3. 
```
SELECT DISTINCT o.boat
FROM p in persons, o in p.ownerships
WHERE p.name = 'Jack Sparrow'
```

4. I wasn't sure how to represent this query, so I am simply ordering by most recently owned starting with nulls and then ordering by end date.
 
```
SELECT o.boat
FROM p in persons, o in p.ownerships
WHERE p.name = 'Jack Sparrow'
ORDERED BY (o.end IS NULL) DESC, o.end DESC
``` 

5.
```
SELECT p 
FROM p in persons
WHERE 
    (SELECT COUNT(DISTINCT b) 
     FROM o in ownerships, b in boats 
     WHERE o.boat = b AND 'Americas Cup' in b.racesWon AND p in o.coOwners)
    = 
    (SELECT COUNT(b) 
     FROM b in boats 
     WHERE 'Americas Cup' in b.racesWon)
```

### III 
#### QBE

1. 

| RacesWon   | raceid | race_name      | boatfk |
|------------|--------|----------------|--------|
|            |        | 'Americas Cup' | P._x   |

| Ownership | ownid | begin_date | end_date | boatfk |
|-----------|-------|------------|----------|--------|
|           | _y    |            |          | _x     |

| CoOwners | coownid | ownid | ssn |
|----------|---------|-------|-----|
|          |         | _y    | P.  |


2.

| Person | ssn | name           | dob |
|--------|-----|----------------|-----|
|        | _x  | 'Jack Sparrow' |     |


| CoOwners | coownid | ownid | ssn |
|----------|---------|-------|-----|
|          |         | _y    | _x  |

| Ownership | ownid | begin_date | end_date | boatfk |
|-----------|-------|------------|----------|--------|
|           | _y    |            |          | P.     |

4.

| Person | ssn | name           | dob |
|--------|-----|----------------|-----|
|        | _x  | 'Jack Sparrow' |     |

| CoOwners | coownid | ownid | ssn |
|----------|---------|-------|-----|
|          |         | _y    | _x  |

| Ownership | ownid | begin_date | end_date | boatfk |
|-----------|-------|------------|----------|--------|
|           | _y    |            | _z       |        |


| CONDITIONS                              |
|-----------------------------------------|
| _z is NULL or (max(_z) and _z NOT NULL) |

5. 

I STAGE

| RacesWon   | raceid | race_name      | boatfk |
|------------|--------|----------------|--------|
|            |        | 'Americas Cup' | _x     |

| Ownership | ownid | begin_date | end_date | boatfk |
|-----------|-------|------------|----------|--------|
|           |  _y   |            |          | _x     |
|  NOT      |  _y   |            |          |        |

| bad owner | ownid |
|-----------|-------|
| I.        | _y    |

II STAGE

| Ownership | ownid | begin_date | end_date | boatfk |
|-----------|-------|------------|----------|--------|
|           |  _x   |            |          |        |

| bad owner | ownid |
|-----------|-------|
| NOT       | _x    |

| result | ownid |
|--------|-------|
| I.     | x     |
