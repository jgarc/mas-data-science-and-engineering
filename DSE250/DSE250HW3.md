# Assignment 3

## Javier Garcia

* Prefixes document for [namespace](https://dbpedia.org/sparql/?help=nsdecl). In my particular case, I got a lot of value in specifying some form of schemas. It made my queries much simplier. In some particular cases, I could not get any output without them.  

1. 

```
PREFIX dbr: <http://dbpedia.org/resource/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dbo: <http://dbpedia.org/ontology/>

SELECT ?property ?value
WHERE {
    dbr:Afghanistan ?property ?value .
    FILTER (LANG(?value) = 'en' || datatype(?value) != rdfs:langString)
    FILTER (STR(?value) != "")
}
```

There are a lot of values without a language set, but all of these unlabeled values are essentially all in English.

Read out of certain properties returned:
```
Aghanistan/Islamic Emirate of Afghanistan/Afghan, with Hasan Akhund as their leader as a UN member state of a unregonized government, is a landlocked country located at the crossroads of Central Asia and South Asia. A population of 38346720 and a Human Development index of 0.478.
```

2. 

```
PREFIX dbr: <http://dbpedia.org/resource/>
PREFIX dbo: <http://dbpedia.org/ontology/>

SELECT ?abstract
WHERE {
    dbr:Katrín_Jakobsdóttir dbo:abstract ?abstract .
    FILTER (LANG(?abstract) = 'en')
}
```

Filter by language to get a single entry that is in English. 

Short summary:
```
Katrín Jakobsdóttir (Icelandic: [ˈkʰaːtʰrin ˈjaːkʰɔpsˌtouʰtɪr̥]; born 1 February 1976) is an Icelandic politician who has been serving as the prime minister of Iceland since 2017 and a member of the Althing for the Reykjavík North constituency since 2007...
```

3. 

```
PREFIX dbr: <http://dbpedia.org/resource/>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>

SELECT ?latitude ?longitude
WHERE {
    dbr:Malta geo:lat ?latitude ;
             geo:long ?longitude .
}
```
Result:
```
latitude: 35.9  longitude: 14.5167
```

4.

```
PREFIX dbr: <http://dbpedia.org/resource/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT ?value
WHERE {
    dbr:Nepal dbo:capital ?capital .
    ?capital rdfs:label ?value .
    FILTER (LANG(?value) = 'en')
}
```
Resukt:
```
Kathmandu.
```

5.

```
PREFIX dbr: <http://dbpedia.org/resource/>
PREFIX dbp: <http://dbpedia.org/property/>

SELECT ?fields
WHERE {
    dbr:Roger_Penrose dbp:fields ?fields .
}
```

Result:
```
Mathematical physics, tessellations
```

6. 

```
PREFIX dbr: <http://dbpedia.org/resource/>
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbp: <http://dbpedia.org/property/>

SELECT ?field ?office
WHERE {
    dbr:Indira_Gandhi dbo:wikiPageWikiLink ?field;
    dbp:office ?office .
    FILTER (?field = dbr:Category:Assassinated_Indian_politicians)
    FILTER (LANG(?office) = 'en')
}

```
Result: 

```
A woman prime minister of India that was assassinated. 
```
💀

