# Homework 4

## Javier Garcia

___

1. Find the years in which movies were relateased.

The output should be a set of integers (no duplicates)

```
SELECT DISTINCT m.movie.year
FROM movies m
```

2. For each movie, find the size of its cast.

The output should be a bag of objects of fields 'title' and 'castSize'.

```
SELECT m.movie.title, COUNT(*) AS castSize 
FROM movies m, m.movie.theCast
GROUP BY m.movie.title
```

3. For each director in the database, find the number of movies they directed.

The output should be a bag of objects with fields 'director' of type string (the name of the director) and 'movieCount' of type integer.

```
SELECT d AS director, COUNT(*) AS movieCount
FROM movies m, m.movie.directors AS d
GROUP BY d
```

4. For each role, find the movies it occurred in.

The output will be a bag of objects, with fields 'role' and 'occurredIn', of type string (the role name) and list of strings (the movie titles). 

```
SELECT c.role,
(SELECT VALUE r.m.movie.title FROM roles r) AS occurredIn
FROM movies AS m, m.movie.theCast AS c
GROUP BY c.role
GROUP AS roles
```

5. Find the actors who have appeared in every movie.

```
SELECT a.actor
FROM movies m, m.movie.theCast AS a
GROUP BY a.actor
HAVING COUNT(DISTINCT m.movie.title) = 
(SELECT COUNT(*) FROM movies)
```
