footer: Javier Garcia
slidenumbers: true

[.footer: Image Source: Riot Games]
![](images/worlds_2022.png)
# Data 200 Final Project
## Analyzing the 2022 League of Legends Worlds Championship

Javier Garcia

---

Worlds Main Event Player Data – [source](https://oracleselixir.com/stats/players/byTournament/2022%20Season%20World%20Championship%2FMain%20Event)[^1]
I was provided a downloadable csv.

![inline](images/worlds_players.png)


[^1]: Dataset is taken from [Oracle's Elixir](https://oracleselixir.com/)


---
Most data here is numerical, with some exceptions for `Player`, `Team`, `Position`. You might notice lots of `object` types, but these are tied to stats that include percentages, so those can be converted to a decimal. 

```python
df[to_num] = df[to_num].str.rstrip('%').astype('float')
```

Otherwise, the data is clean.

Detailed List of definitions in the dataset – [link](https://oracleselixir.com/definitions)

### Some notable features:

1. `EGPM` – Average earned gold per minute
1. `DPM`– Damage per minute
1. `K` – Total Kills

![left](images/worlds_players_info.png)

---

Champions Queue Tournament Ladder  – [source](https://oracleselixir.com/cq/leaderboard) [^1]

Ranked queue where current and prospective professional players from North America can get good practice.

![inline](images/tim-email.png)

This data was [web scraped from HTML](https://convertio.co/html-csv/), then formatted/cleaned with `csv`

I can provide a Google Drive Link of the [raw scraped CSV](https://drive.google.com/file/d/148STcjMbGr5o3s4xHsS-K51FrGGGWJ-i/view?usp=sharing), as well as the [final CSV](https://drive.google.com/file/d/1WI4TPMg06deNUHNlT-DSZxBly3eIIM21/view?usp=sharing), as well as the raw data and the code to transform the data. 

[^1]: Dataset is taken from [Oracle's Elixir](https://oracleselixir.com/)

---
[.column]

![inline](images/champs_queue.png) 

![inline](images/champs_queue_info.png)

[.column]

Players that aren't playing for a team will have a '-' for Team Value. 68 players, or about 1 in 4 players won't be signed to a team.

### Notable features:

1. `LP` – Ladder Points, a League of Legends-specific variant to ELO from practice games against other pros
1. `OE` – Oracle’s Elixir Performance Rating (Letter grade)

---

![inline](images/wp-1.jpg)

Supports are placing lots of wards while carries are getting lots of kills. It's hard to do both.

---

![inline](images/wp-2.jpg)

Prominent negative relationship. Possible explanations: Carries have their less rich teammates purchase vision. Losing teams use vision as a method for coming back into losing games.

---

![inline](images/cq-1.jpeg)

Players with higher LP have more games under their belt. This could point to more practice leading to better outcomes.

---
# Merging the two Dataframes

![inline](images/data-set-merge.png)

---

![inline](images/mdf-1.jpg)

---

## Not all players who participate in tournaments are known to use Champions Queue. This begs the question of:  

1. Could you come up with LP values based on tournament games? How does that differ based on the position that you're playing?
1. Could you use the LP values of various players to predict the percentage chance of `Team A` winning against `Team B`?
