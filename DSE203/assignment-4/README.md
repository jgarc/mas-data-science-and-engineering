# Assignment 4

The `JedAI-dirty-ER.ipynb` and `dedupe.ipynb` notebook contains an exploration of the following assignments. 


`JedAI.ipynb` contains the notebook with the dirty ER tutorial, but gave us very few entity results.

We used `csv_example.py` from the `dedupe` documentation and adjusted it to our dataset. Console output of active learning process given to the users in `console-output.txt`. `csv_example_learned_settings` are learning params saved to disk.  

As a first pass, I feel that JedAI did not establish enough entity resolution relationships, where dedupe did so. This is likely due to the params that I gave, meaning we should continue to fine tune that. 

I'm interested to see how long the active learning process should take given the size of our data set for `dedupe`. It's hard to tell whether two records are the same person or not. Some feel like they could be typos. Another scenario I had to resolve whether there were child/parent who live in the same house based on the birth dates. I felt more willing to be "unsure" if two results were the same person the more I did it. I saw a lot more matching records than not. I am wondering if that is correct.

What does the ground truth of this dataset look like? In other words, how do I know if the entity resolution process is correct? 
