/* HW 1: The 201Cats web application provides sophisticated cat video viewing to its users. Each user has a user
name and logs in the 201Cats using her Facebook log-in. Consequently, the company regularly obtains
information of which ones of the 201Cats users are Facebook followers of other 201Cats users.
When a user logs in, the web application suggests to her 10 cat videos – more on this below. The user
may

● Watch one of the suggested videos (multiple times)
● Like a suggested video; may like a video even without watching it. A user may like a video just
once. Clicking many times on the like does not result in “liking many times”.

The 201Cats database captures the following information, with minimum redundancy:

● The user’s name and Facebook login – password not needed.
● The user’s “like” activity: store which video were liked and when.
● The user’s “watch” activity: store which videos were watched and when.
● The times the user logged in and the videos that were suggested to the user to watch when she
logged in.
● Which 201Cats users are friends of each user. You are allowed some redundancy here: It is OK if
the database captures both that “X is friend of Y” and “Y is friend of X”, despite the fact that this
is redundant since Facebook friendships are symmetric.

Produce an SQL schema that captures the above information. Optionally (and not graded), submit the
corresponding E/R design – if you designed the schema using the E/R technique (something we
recommend highly).
*/

CREATE TABLE users(
	uid SERIAL PRIMARY KEY,
	username text NOT NULL,
	facebook_login text NOT NULL
);

CREATE TABLE friends(
	fid SERIAL PRIMARY KEY, 
	friends_with INTEGER REFERENCES users(uid) NOT NULL, 
	friends_of INTEGER REFERENCES users(uid) NOT NULL
);

CREATE TABLE videos(
	vid SERIAL PRIMARY KEY,
	vname text NOT NULL
);

CREATE TABLE watch(
	wid SERIAL PRIMARY KEY,
	uid INTEGER REFERENCES users(uid) NOT NULL,
	vid INTEGER REFERENCES videos(vid) NOT NULL,
	time_watched TIMESTAMP NOT NULL
);

CREATE TABLE likes(
	lid SERIAL PRIMARY KEY,
	uid INTEGER REFERENCES users(uid) NOT NULL,	
	vid INTEGER REFERENCES videos(vid) NOT NULL,
	time_liked TIMESTAMP NOT NULL
);

CREATE TABLE login(
	login_id SERIAL PRIMARY KEY,
	uid INTEGER REFERENCES users(uid) NOT NULL,
	login_time TIMESTAMP NOT NULL
);

CREATE TABLE recommended_videos(
	rid SERIAL PRIMARY KEY,
	login_id INTEGER REFERENCES login(login_id) NOT NULL,
	vid INTEGER REFERENCES videos(vid) NOT NULL
);
