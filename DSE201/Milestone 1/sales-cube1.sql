/* HW1: Consider a database that captures customers with a name and state (of residence); states with a name;
products that have a list price, a name and belong to a category; categories have names and
descriptions; sales of a product to a customer capturing quantity and percent discount from list price (a
product may be discounted on a case-by-case basis). */

CREATE TABLE categories(
	id SERIAL PRIMARY KEY,
	cname TEXT NOT NULL,
	cdescr TEXT NOT NULL
);

CREATE TABLE states(
	sid SERIAL PRIMARY KEY,
	stname TEXT NOT NULL,
	stabrrev CHARACTER(2) NOT NULL
);

CREATE TABLE products(
 	pid SERIAL PRIMARY KEY,
	pname TEXT,
	price decimal(12,2),
	cid INTEGER REFERENCES categories(id) NOT NULL
);

CREATE TABLE customers(
	cid SERIAL PRIMARY KEY,
	fname TEXT NOT NULL,
	lname TEXT NOT NULL,
	state INTEGER REFERENCES states(sid) NOT NULL
);

CREATE TABLE sales(
	id SERIAL PRIMARY KEY,
	pid INTEGER REFERENCES products(pid) NOT NULL,
	cid INTEGER REFERENCES customers(cid) NOT NULL,
	quantity INTEGER CHECK (quantity >= 0),
	discount FLOAT NOT NULL DEFAULT 0 CHECK (discount BETWEEN 0.0 and 1.0)
);
