# Cats Milestone 3

## Statistics

I have seven tables; five are used in queries. Using the schema below, I can find the size on disk of the table contents. 

```sql
SELECT 
    relname AS "Table", 
    pg_size_pretty(pg_total_relation_size(relid)) AS "Size"
FROM 
    pg_catalog.pg_statio_user_tables 
WHERE 
    relname = 'table_name_here'
ORDER BY 
    pg_total_relation_size(relid) DESC;
```

| table name | tuples   | size(in MB) |
|------------|----------|-------------|
| videos     | 1000000  | 71          |
| users      | 266397   | 19          |
| friends    | 13215294 | 841         |
| likes      | 13173434 | 839         |
| watch      | 13188649 | 840         |

## Query Performance before index

To verify the results are accurate, I ran these queries with cold starts three times and took the median resulting time. I am running MacOS 13.2.1 using an Apple M1 Max, with 32 GB of RAM.

| query  | time(in sec) |
|--------|--------------|
| 2.1    | 7.079        |
| 2.2    | 2.855        |
| 2.3    | 4.361        |
| 2.4    | 2.914        |
| 2.5    | 3.156        |

## Indexes Created 

```sql
--- MILESTONE 3
CREATE INDEX likes_uid_idx ON likes(uid);
CREATE INDEX likes_vid_idx ON likes(vid);

CREATE INDEX watch_uid_idx ON watch(uid);
CREATE INDEX watch_vid_idx ON watch(vid);

CREATE INDEX friends_with_idx ON friends(friends_with);
CREATE INDEX fiends_of_idx ON friends(friends_of);

CREATE INDEX recommended_login_idx ON recommended_videos(login_id);
CREATE INDEX recommended_vid ON recommended_videos(vid);
```

## Query Performance after index

| query  | time(in sec) | % improvement |
|--------|--------------|---------------|
| 2.1    | 34.985       |               |
| 2.2    | 0.204        |               |
| 2.3    | 0.114        |               |
| 2.4    | 0.102        |               |
| 2.5    | 0.140        |               |

### Why did performance improve 

Most of the output here is what I would expect. All of the queries are seeing signigicant performance improvements. However, the timing of my first query looks to be doing siginicantly worse. I need to do further research as to why this is the case.