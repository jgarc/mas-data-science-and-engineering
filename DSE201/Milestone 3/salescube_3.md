# Salescube Milestone 3

## Statistics

I have five tables. Using a schema below, I can find the size needed to hold all of the table contents. 

```sql
SELECT 
    relname AS "Table", 
    pg_size_pretty(pg_total_relation_size(relid)) AS "Size"
FROM 
    pg_catalog.pg_statio_user_tables 
WHERE 
    relname = 'table_name_here'
ORDER BY 
    pg_total_relation_size(relid) DESC;
```

| table name | tuples  | size(in MB) |
|------------|---------|-------------|
| states     | 50      | 0.032       |
| users      | 100     | 0.04        |
| customers  | 887990  | 65          |
| products   | 100000  | 7.344       |
| sales      | 8433575 | 600         |

## Query Performance before index

To verify the results are accurate, I ran these queries with cold starts three times and took the median resulting time. I am running MacOS 13.2.1 using an Apple M1 Max, with 32 GB of RAM.

| query  | time(in sec) |
|--------|--------------|
| 2.1    | 6.524        |
| 2.2    | 3.610        |
| 2.3    | 6.708        |
| 2.4    | 6.909        |
| 2.5    | 0.370        |
| 2.6    | 13.331       |

## Indexes Created 

```sql
--- MILESTONE 3
CREATE INDEX customers_cid_idx ON customers(cid);
CREATE INDEX customers_state_idx ON customers(state);

CREATE INDEX sales_cid_idx ON sales(cid);
CREATE INDEX sales_pid_idx ON sales(pid);
CREATE INDEX sales_qty_idx ON sales(quantity);
CREATE INDEX sales_discount_idx ON sales(discount DESC);

CREATE INDEX products_pid_idx ON products(pid);
CREATE INDEX products_cid_idx ON products(cid);
CREATE INDEX products_price_idx ON products(price);
```

## Query Performance after index

| query  | time(in sec) | % improvement |
|--------|--------------|---------------|
| 2.1    | 6.202        |               |
| 2.2    | 3.512        |               |
| 2.3    | 6.574        |               |
| 2.4    | 7.035        |               |
| 2.5    | 0.194        |               |
| 2.5    | 13.074       |               |

### Why did performance improve 

Above, I listed the final version of the indexes that I had tried. At first, I was puzzled by the unaffected performance of my queries. However, I realized that all of my SQL queries must traverse most (in some cases all) of the data before receiving the output. The tuples executed in a different order won't improve performance. As a result of adding those indexes, I should expect performance to be worse, and therefore removed the indexes I had created.

If I had implemented query three to be for a given customer instead of for all customers, I should see an improvement. With the queries I already have, I would see further performance improvement in writing better SQL queries themselves. 


