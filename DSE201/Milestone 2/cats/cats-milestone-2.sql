--- MILSTONE 1
--- CREATE TABLES 
--- note that to run some queries, I needed to remove the NOT NULL constraints on some values
CREATE TABLE users(
	uid SERIAL PRIMARY KEY,
	username text NOT NULL,
	facebook_login text NOT NULL
);

CREATE TABLE friends(
	fid SERIAL PRIMARY KEY, 
	friends_with INTEGER REFERENCES users(uid) NOT NULL, 
	friends_of INTEGER REFERENCES users(uid) NOT NULL
);

CREATE TABLE videos(
	vid SERIAL PRIMARY KEY,
	vname text NOT NULL
);

CREATE TABLE watch(
	wid SERIAL PRIMARY KEY,
	uid INTEGER REFERENCES users(uid) NOT NULL,
	vid INTEGER REFERENCES videos(vid) NOT NULL,
	time_watched TIMESTAMP NOT NULL
);

CREATE TABLE likes(
	lid SERIAL PRIMARY KEY,
	uid INTEGER REFERENCES users(uid) NOT NULL,	
	vid INTEGER REFERENCES videos(vid) NOT NULL,
	time_liked TIMESTAMP NOT NULL
);

CREATE TABLE login(
	login_id SERIAL PRIMARY KEY,
	uid INTEGER REFERENCES users(uid) NOT NULL,
	login_time TIMESTAMP NOT NULL
);

CREATE TABLE recommended_videos(
	rid SERIAL PRIMARY KEY,
	login_id INTEGER REFERENCES login(login_id) NOT NULL,
	vid INTEGER REFERENCES videos(vid) NOT NULL
);

--- MILSTONE 2
--- Helper function: Check if a user has watched or liked a vid 
--- this function is particular to postgres and would need to be adapted in other systems
CREATE FUNCTION user_video_interacted(uid INTEGER)
RETURNS TABLE(videos_liked INTEGER) AS $$
BEGIN
 	RETURN QUERY
	  SELECT vid FROM likes AS l
	  WHERE l.uid = $1
	  UNION 
	  SELECT vid FROM watch as w
	  WHERE w.uid=$1;
END;
$$ LANGUAGE plpgsql;

--- CATS 2.1 Overall Likes
SELECT vid, COUNT(vid) FROM likes AS l 
WHERE NOT EXISTS (
	SELECT * FROM user_video_interacted(6873) AS v WHERE v.videos_liked = l.vid --- update here
)
GROUP BY l.vid
ORDER BY l.count DESC
LIMIT 10;

--- CATS 2.2 Friends Likes
PREPARE cats_2_2(int) AS
	SELECT vid, count(vid)
	FROM ( 
		SELECT friends.friends_of FROM friends 
		WHERE friends.friends_with = $1
	) AS f JOIN likes AS l ON f.friends_of = l.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = l.vid
	)
	GROUP BY l.vid
	ORDER BY count DESC
	LIMIT 10;

EXECUTE cats_2_2(0); --- update here

--- CATS 2.3 Friends of Friends Likes
PREPARE cats_2_3(int) AS
	SELECT likes.vid, COUNT(likes.vid)
	FROM ( 
		SELECT friends.friends_of FROM
		(
			SELECT friends.friends_of FROM friends 
			WHERE friends.friends_with = $1
		) AS mutual_friends JOIN friends ON mutual_friends.friends_of = friends.friends_with
		UNION
		SELECT friends.friends_of FROM friends 
		WHERE friends.friends_with = $1
	) AS all_friends JOIN likes ON all_friends.friends_of = likes.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = likes.vid
	)
	GROUP BY likes.vid
	ORDER BY count DESC
	LIMIT 10;

EXECUTE cats_2_3(0); --- update here

--- CATS 2.4 My kind of cats
PREPARE cats_2_4(int) AS
	SELECT likes.vid, count(likes.vid)
	FROM (
		SELECT DISTINCT likes.uid from likes 
		WHERE likes.vid in(
			SELECT vid FROM likes
			where likes.uid = $1
		)
	) AS liked_common JOIN likes ON liked_common.uid = likes.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = likes.vid
	)
	GROUP BY likes.vid
	ORDER BY count DESC
	LIMIT 10;

EXECUTE cats_2_4(0); --- update here

--- CATS 2.5 My kind of cats with preference
PREPARE cats_2_5(int) AS
	SELECT likes.vid, SUM(user_weights.lc) as total_lc from likes 
	RIGHT JOIN (
		SELECT likes.uid, LOG(COUNT(likes.vid) + 1) AS lc from likes 
		WHERE likes.vid IN (
			SELECT likes.vid FROM likes
			WHERE likes.uid = $1
		) AND NOT likes.uid = $1
		GROUP BY likes.uid
	) AS user_weights
	ON likes.uid = user_weights.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = likes.vid
	)
	GROUP BY likes.vid
	ORDER BY total_lc DESC
	LIMIT 10;

EXECUTE cats_2_5(7010); --- update here

--- MILESTONE 3

--- likes
CREATE INDEX likes_uid_idx ON likes(uid);
CREATE INDEX likes_vid_idx ON likes(vid);

--- watch
CREATE INDEX watch_uid_idx ON watch(uid);
CREATE INDEX watch_vid_idx ON watch(vid);

--- friends
CREATE INDEX friends_with_idx ON friends(friends_with);
CREATE INDEX fiends_of_idx ON friends(friends_of);

--- recommended videos
CREATE INDEX recommended_login_idx ON recommended_videos(login_id);
CREATE INDEX recommended_vid ON recommended_videos(vid);


