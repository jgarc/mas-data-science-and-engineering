-- Query with all indexes
EXPLAIN SELECT vid, COUNT(vid) FROM likes AS l 
WHERE NOT EXISTS (
    SELECT * FROM user_video_interacted(6873) AS v WHERE v.videos_liked = l.vid
)
GROUP BY l.vid
ORDER BY l.count DESC
LIMIT 10;

-- Query without the likes_vid_idx index
DROP INDEX likes_vid_idx;
EXPLAIN SELECT vid, COUNT(vid) FROM likes AS l 
WHERE NOT EXISTS (
    SELECT * FROM user_video_interacted(6873) AS v WHERE v.videos_liked = l.vid
)
GROUP BY l.vid
ORDER BY l.count DESC
LIMIT 10;

DROP INDEX recommended_login_idx;
DROP INDEX recommended_vid

EXPLAIN SELECT vid, COUNT(vid) FROM likes AS l 
WHERE NOT EXISTS (
    SELECT * FROM user_video_interacted(6873) AS v WHERE v.videos_liked = l.vid
)
GROUP BY l.vid
ORDER BY l.count DESC
LIMIT 10;

-- CREATE INDEX watch_uid_idx ON watch(uid);
-- CREATE INDEX watch_vid_idx ON watch(vid);

-- CREATE INDEX friends_with_idx ON friends(friends_with);
-- CREATE INDEX fiends_of_idx ON friends(friends_of);

-- CREATE INDEX recommended_login_idx ON recommended_videos(login_id);
-- CREATE INDEX recommended_vid ON recommended_videos(vid);
SELECT relname AS table_name, indexrelname AS index_name, idx_scan, idx_tup_read, idx_tup_fetch
FROM pg_stat_user_indexes;


SELECT l.vid, COUNT(l.*) 
FROM likes AS l 
WHERE l.vid NOT IN (
  SELECT videos_liked FROM user_video_interacted(6873) ORDER BY interactions DESC LIMIT 10
)
GROUP BY l.vid
ORDER BY COUNT(l.*) DESC
LIMIT 10;


EXPLAIN SELECT vid, COUNT(vid) FROM likes AS l 
WHERE NOT EXISTS (
    SELECT 1 FROM user_video_interacted(6873) AS v WHERE v.videos_liked = l.vid
)
GROUP BY l.vid
ORDER BY COUNT(*) DESC
LIMIT 10;