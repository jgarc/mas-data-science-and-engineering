--- CATS 2.2
PREPARE cats_2_2(int) AS
	SELECT vid, count(vid)
	FROM ( 
		SELECT friends.friends_of FROM friends 
		WHERE friends.friends_with = $1
	) AS f JOIN likes AS l ON f.friends_of = l.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = l.vid
	)
	GROUP BY l.vid
	ORDER BY count DESC
	LIMIT 10;

EXECUTE cats_2_2(0);
