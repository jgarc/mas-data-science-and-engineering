--- CATS 2.3
-- DEALLOCATE cats_2_3;
PREPARE cats_2_3(int) AS
	SELECT likes.vid, COUNT(likes.vid)
	FROM ( 
		SELECT friends.friends_of FROM
		(
			SELECT friends.friends_of FROM friends 
			WHERE friends.friends_with = $1
		) AS mutual_friends JOIN friends ON mutual_friends.friends_of = friends.friends_with
		UNION
		SELECT friends.friends_of FROM friends 
		WHERE friends.friends_with = $1
	) AS all_friends JOIN likes ON all_friends.friends_of = likes.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = likes.vid
	)
	GROUP BY likes.vid
	ORDER BY count DESC
	LIMIT 10;

EXECUTE cats_2_3(0); --- update here