--- CATS 2.5
PREPARE cats_2_5(int) AS
	SELECT likes.vid, SUM(user_weights.lc) as total_lc from likes 
	RIGHT JOIN (
		SELECT likes.uid, LOG(COUNT(likes.vid) + 1) AS lc from likes 
		WHERE likes.vid IN (
			SELECT likes.vid FROM likes
			WHERE likes.uid = $1
		) AND NOT likes.uid = $1
		GROUP BY likes.uid
	) AS user_weights
	ON likes.uid = user_weights.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = likes.vid
	)
	GROUP BY likes.vid
	ORDER BY total_lc DESC
	LIMIT 10;

EXECUTE cats_2_5(7010); --- update here

