--- Check if a user has watched or liked a vid 
DROP FUNCTION IF EXISTS user_video_interacted(INT);
CREATE FUNCTION user_video_interacted(uid INTEGER)
RETURNS TABLE(videos_liked INTEGER) AS $$
BEGIN
 	RETURN QUERY
	  SELECT vid FROM likes AS l
	  WHERE l.uid = $1
	  UNION 
	  SELECT vid FROM watch as w
	  WHERE w.uid=$1;
END;
$$ LANGUAGE plpgsql;

--- cats 2.1
SELECT vid, COUNT(vid) FROM likes AS l 
WHERE NOT EXISTS (
	SELECT * FROM user_video_interacted(6873) AS v WHERE v.videos_liked = l.vid
)
GROUP BY l.vid
ORDER BY l.count DESC
LIMIT 10;
