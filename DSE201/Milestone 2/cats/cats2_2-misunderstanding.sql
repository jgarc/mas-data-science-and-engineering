--- liked videos from my friends
SELECT vid, COUNT(vid) FROM likes
WHERE vid IN (
	SELECT likes.vid
	FROM ( 
		SELECT friends.friends_of FROM friends 
		WHERE friends.friends_with = 0
	) AS subquery JOIN LIKES ON subquery.friends_of = likes.uid
) AND NOT EXISTS (
	SELECT * FROM user_video_interacted(0) AS v WHERE v.videos_liked = likes.vid
)
GROUP BY vid
ORDER BY count DESC
LIMIT 10;


