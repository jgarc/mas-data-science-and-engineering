--- CATS 2.4
PREPARE cats_2_4(int) AS
	SELECT likes.vid, count(likes.vid)
	FROM (
		SELECT DISTINCT likes.uid from likes 
		WHERE likes.vid in(
			SELECT vid FROM likes
			where likes.uid = $1
		)
	) AS liked_common JOIN likes ON liked_common.uid = likes.uid
	WHERE NOT EXISTS (
		SELECT * FROM user_video_interacted($1) AS v WHERE v.videos_liked = likes.vid
	)
	GROUP BY likes.vid
	ORDER BY count DESC
	LIMIT 10;

EXECUTE cats_2_4(0); --- update here