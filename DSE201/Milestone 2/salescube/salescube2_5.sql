SELECT customers.state, categories.cname,
SUM(sales.quantity * products.price * (1 - sales.discount))::decimal(12,2) as total 
FROM sales
	JOIN products 
		ON sales.pid = products.pid 
	JOIN categories
		ON products.pid = categories.id
	JOIN customers 
		ON sales.cid = customers.cid 
GROUP BY customers.state, categories.cname
