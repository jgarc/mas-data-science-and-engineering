SELECT products.pid, sales.cid as cust_id,
(sales.quantity * products.price * (1 - sales.discount))::decimal(12,2) as total 
FROM sales
JOIN products on sales.pid = products.pid
WHERE (sales.quantity * products.price * sales.discount) > 0
ORDER BY total DESC