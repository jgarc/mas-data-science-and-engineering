--- MILESTONE 1
CREATE TABLE categories(
	id SERIAL PRIMARY KEY,
	cname TEXT NOT NULL,
	cdescr TEXT NOT NULL
);

CREATE TABLE states(
	sid SERIAL PRIMARY KEY,
	stname TEXT NOT NULL,
	stabrrev CHARACTER(2) NOT NULL
);

CREATE TABLE products(
 	pid SERIAL PRIMARY KEY,
	pname TEXT,
	price decimal(12,2),
	cid INTEGER REFERENCES categories(id) NOT NULL
);

CREATE TABLE customers(
	cid SERIAL PRIMARY KEY,
	fname TEXT NOT NULL,
	lname TEXT NOT NULL,
	state INTEGER REFERENCES states(sid) NOT NULL
);

CREATE TABLE sales(
	id SERIAL PRIMARY KEY,
	pid INTEGER REFERENCES products(pid) NOT NULL,
	cid INTEGER REFERENCES customers(cid) NOT NULL,
	quantity INTEGER CHECK (quantity >= 0),
	discount FLOAT NOT NULL DEFAULT 0 CHECK (discount BETWEEN 0.0 and 1.0)
);

--- MILESTONE 2

---2.1
SELECT customers.cid, SUM(COALESCE(sales.quantity, 0)),
SUM(COALESCE((sales.quantity * products.price * (1 - sales.discount))::decimal(12,2),0)) as total 
FROM sales
JOIN products ON sales.pid = products.pid
RIGHT JOIN customers ON customers.cid = sales.cid
GROUP BY customers.cid

---2.2
SELECT customers.state, COALESCE(SUM(foo.quantity), 0) AS quantity, 
COALESCE(SUM(foo.total), 0) AS total
FROM customers 
LEFT JOIN (SELECT sales.cid, products.pid, sales.quantity,
		  (sales.quantity * products.price * (1 - sales.discount))::decimal(12,2) AS total 
	   FROM sales
	   JOIN products ON sales.pid = products.pid) AS foo ON customers.cid = foo.cid
GROUP BY customers.state;

---2.3
SELECT products.pid, sales.cid as cust_id,
(sales.quantity * products.price * (1 - sales.discount))::decimal(12,2) as total 
FROM sales
JOIN products on sales.pid = products.pid
WHERE (sales.quantity * products.price * sales.discount) > 0
ORDER BY total DESC

---2.4
SELECT customers.cid, foo.pid, foo.total
FROM customers 
JOIN (SELECT sales.cid, products.pid, sales.quantity,
		(sales.quantity * products.price * (1 - sales.discount))::decimal(12,2) as total 
		FROM sales
		JOIN products on sales.pid = products.pid)
	as foo ON customers.cid = foo.cid
ORDER BY total DESC

---2.5
SELECT customers.state, categories.cname,
SUM(sales.quantity * products.price * (1-sales.discount))::decimal(12,2) as total 
FROM sales
	JOIN products 
		ON sales.pid = products.pid 
	JOIN categories
		ON products.pid = categories.id
	JOIN customers 
		ON sales.cid = customers.cid 
GROUP BY customers.state, categories.cname

---2.6
SELECT category_id, customer_id, total_quantity, total_sales
FROM (
	SELECT products.cid AS category_id, sales.cid AS customer_id, 
	       SUM(sales.quantity) AS total_quantity, 
	       SUM(sales.quantity * products.price * (1-sales.discount))::decimal(12,2) AS total_sales, 
	       ROW_NUMBER() OVER (PARTITION BY products.cid ORDER BY SUM(sales.quantity * products.price * (1-sales.discount)) DESC) AS rank
	FROM sales
		JOIN products 
			ON sales.pid = products.pid
	GROUP BY products.cid, sales.cid
) AS customer_totals
WHERE rank <= 20;

--- MILESTONE 3

---2.1
CREATE INDEX sales_pid_idx ON sales(pid);
CREATE INDEX customers_cid_idx ON customers(cid);

---2.2
CREATE INDEX sales_cid_idx ON sales(cid);
CREATE INDEX products_pid_idx ON products(pid);
CREATE INDEX customers_state_idx ON customers(state);

---2.3
CREATE INDEX sales_quantity_idx ON sales(quantity);
CREATE INDEX products_price_idx ON products(price);
CREATE INDEX sales_discount_idx ON sales(discount);

---2.4
--- CREATE INDEX sales_cid_idx ON sales(cid);
--- CREATE INDEX products_pid_idx ON products(pid);
--- CREATE INDEX customers_cid_idx ON customers(cid);

---2.5
--- CREATE INDEX sales_cid_idx ON sales(cid);
--- CREATE INDEX products_pid_idx ON products(pid);
CREATE INDEX categories_id_idx ON categories(id);
--- CREATE INDEX customers_state_idx ON customers(state);

---2.6
--- CREATE INDEX sales_pid_idx ON sales(pid);
--- CREATE INDEX products_price_idx ON products(price);
--- CREATE INDEX sales_discount_idx ON sales(discount);

