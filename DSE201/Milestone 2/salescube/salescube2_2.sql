SELECT customers.state, COALESCE(SUM(foo.quantity), 0) AS quantity, 
COALESCE(SUM(foo.total), 0) AS total
FROM customers 
LEFT JOIN (SELECT sales.cid, products.pid, sales.quantity,
		  (sales.quantity * products.price * (1 - sales.discount))::decimal(12,2) AS total 
	   FROM sales
	   JOIN products ON sales.pid = products.pid) AS foo ON customers.cid = foo.cid
GROUP BY customers.state;
