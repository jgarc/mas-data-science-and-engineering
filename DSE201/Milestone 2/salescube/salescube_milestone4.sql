SELECT category_id, customer_id, total_quantity, total_sales
FROM (
	SELECT products.cid AS category_id, sales.cid AS customer_id, 
	       SUM(sales.quantity) AS total_quantity, 
	       SUM(sales.quantity * products.price * (1-sales.discount))::decimal(12,2) AS total_sales, 
	       ROW_NUMBER() OVER (PARTITION BY products.cid ORDER BY SUM(sales.quantity * products.price * (1-sales.discount)) DESC) AS rank
	FROM sales
		JOIN products 
			ON sales.pid = products.pid
	GROUP BY products.cid, sales.cid
) AS customer_totals
WHERE rank <= 20;

CREATE MATERIALIZED VIEW customer_totals_2 AS
SELECT products.cid AS category_id, sales.cid AS customer_id, 
       SUM(sales.quantity) AS total_quantity, 
       SUM(sales.quantity * products.price * (1-sales.discount))::decimal(12,2) AS total_sales, 
       ROW_NUMBER() OVER (PARTITION BY products.cid ORDER BY SUM(sales.quantity * products.price * (1-sales.discount)) DESC) AS rank
FROM sales
JOIN products ON sales.pid = products.pid
GROUP BY products.cid, sales.cid;

select category_id, customer_id, total_quantity, total_sales from customer_totals_2 where rank <=20