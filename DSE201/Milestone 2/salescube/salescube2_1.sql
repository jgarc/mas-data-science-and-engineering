SELECT customers.cid, SUM(COALESCE(sales.quantity, 0)),
SUM(COALESCE((sales.quantity * products.price * (1 - sales.discount))::decimal(12,2),0)) as total 
FROM sales
JOIN products ON sales.pid = products.pid
RIGHT JOIN customers ON customers.cid = sales.cid
GROUP BY customers.cid
--- cast(column_name as data_type)
