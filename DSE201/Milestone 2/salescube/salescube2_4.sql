SELECT customers.cid, foo.pid, foo.total
FROM customers 
JOIN (SELECT sales.cid, products.pid, sales.quantity,
		(sales.quantity * products.price * (1 - sales.discount))::decimal(12,2) as total 
		FROM sales
		JOIN products on sales.pid = products.pid)
	as foo ON customers.cid = foo.cid
ORDER BY total DESC