# NYC taxi analysis on green and yellow taxis for May 2022

## Description

For the scope of our research, we will be taking a look at monthly trip records of MAY 2022 in the form of parquet files for yellow and green taxis.


## Table of Contents

1. [Data](#data)
1. [Dask Technologies Used](#technologies-used)
1. [Results](#results)

## Data

We grabbed our data from the (Taxi & Limousine Commission)[https://www.nyc.gov/site/tlc/about/tlc-trip-record-data.page].

In the `data` folder, you'll find a `data/raw_data` directory includes the parquet files that we grabbed. The `post_processed` folder contains data files that have been modified by us in the `Preprocessing.ipynb` notebook. You find data files in there from the rest of 2022 as well. These files were used for using `papermill` to try to test our model against other months' data. The data files follow a generic structure like this: 

```python
files = f'{taxi_color}_tripdata_2022-{month}'
```

For our analysis we focused on the following data files in the `data/raw_data` directory: 
    - `green_tripdata_2022-05.parquet`
    - `yellow_tripdata_2022-05.parquet`
    
After running the preprocessing notebook, the data folder should have two new files in the `data/post_processed` directory:
    - `post_processed/green_tripdata_2022-05_preprocessed`
    - `post_processed/green_tripdata_2022-05_preprocessed`

Some of the names of the features are self-explanatory, while others are not. For more information on what the features mean, refer to the data definitions for the [green](https://www.nyc.gov/assets/tlc/downloads/pdf/data_dictionary_trip_records_green.pdf) and [yellow](https://www.nyc.gov/assets/tlc/downloads/pdf/data_dictionary_trip_records_yellow.pdf) taxi dataset.


## Dask Technologies Used
* `Client`
* `ParallelPostFit`
* `SelectKBest`
* `XGBRegressor`

## Order to run notebooks
Run through the notebooks in the following order:

    1. Preprocessing.ipynb
    2. Green Model.ipynb
    3. Yellow Model.ipynb
    4. Yellow Model with SKB.ipynb
    5. Yellow Model with ParallelPostFit.ipynb
    6. Results.ipynb
    
We also included other notebooks that are supplimental to what we worked on. Those include

* `papermill.ipynb` with parameterized notebooks in the `Papermill` directory
* `Green Model with XGB Regressor.ipynb`
* `Yellow Model with XGB Regressor.ipynb`

After running each notebook, please shut down the notebook's kernel before opening the next notebook as this will prevent problems with the kernel automatically shutting down and restarting

## Results
Results Folder
  - after running all the model notebooks, the Results folder should have four files:
  - green_model.csv
  - yellow_model.csv
  - yellow_model_skb.csv
  - yellow_model_ppf.csv
