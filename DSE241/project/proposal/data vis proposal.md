slidenumbers: true
footer: Javier Garcia

# Transit ridership in San Diego for transportation experts

### Car dependency is a problem.

In North America, the costs of maintaining roads in monetary policy, environmental impact, and public health are unsustainable. 

> “We've spent two generations transforming a public realm once comprised of walkable, mixed-use neighborhoods into auto-only zones. These are places where the kids used to play ball in the street. Today a kid can't even play safely in their own front yard … Imagine two 9/11 attacks each year that killed just kids and you still would not have the number of child fatalities America has each year from auto accidents” [^1]

^ ## Environmental effects
^ * Emissions from vehicles
^ * Construction of roads impacting wildlife
^ * Affects climate: (Concrete/Asphalt not being particularly great with getting rid of rain)
^ ## Costs
^ * True cost of vehicle ownership: According to AAA on average new vehicles driven 15,000 miles costs are $12,182 a year. 
^ trend of heavier cars on the road wear roads more, linked to higher proportion of car accidents being fatal
^ * Wider roads, more asphalt, people drive faster, they're harder to maintain, etc. this is a cycle that feeds into itself
^ ## Health effects
^ * Pollutants from private vehicles 
^ * Lack of passive exercise, linkage between car usage and health outcomes
^ * Mental health effects to super-commuters, traffic 


[^1]: Charles L. Marohn Jr., Thoughts on Building Strong Towns, Volume 1

---
> “We have largely convinced ourselves that transportation spending creates jobs, opportunity and wealth for Americans. Does it? This is a testable hypothesis where we can generate and obtain plenty of data, yet we don’t. Why?” [^1]


### Give public transportation experts and policymakers better ways to *explore and interpret their data.*
1. Transportation designers need to have better tools to understand the ridership data that they have.  
1. Advocate for stronger transportation investment and evangelize ridership.
1. Help decision-makers find strong cases for continued funding. i.e. Adding stops, and expanding frequency. 


^ National League of Cities/ NYT : Multiple research studies show a link between access to transportation and economic mobility
^ Why did I subject this class to a rant about my feelings about the state of transportation in this country? Well because I think this has room to change. 
^ Giving people in power a fighting chance to change stuff 
^ Maybe educate some folks in this class about this topic and consider voting to support transportation initiatives 

[^1]: Charles L. Marohn Jr., Thoughts on Building Strong Towns, Volume 1

---

[.autoscale: true]

# Data sources: 

* [SOC - Transit Ridership by Route + Stop (SANDAG)](https://opendata.sandag.org/Transportation/SOC-Transit-Ridership-by-Route-Stop-SANDAG-/b9mw-mf3n/about_data)[^2]
  * 31116 entries, 22 fields (4.6 MB)
  * Notable fields:
     * Ridership data: Gross number of trips, Sum of Passenger Miles, Length from the previous stop, Number of boardings *and* alightings at stops, Route names
     * Metadata: Year (2019-2022), Direction of trips, Route Number, Service Mode
* [Transit Stop Locations](https://data.sandiego.gov/datasets/transit-stops/)[^3]
 * 6213 entries, 14 fields (1 MB)  
 * Notable fields: 
     * Stop Information: Longitude, Latitude, Stop IDs, Wheelchair(scoring)
     * GeoJSON: outlines the city boundaries (2.9 MB)

[^2]: https://opendata.sandag.org/Transportation/SOC-Transit-Ridership-by-Route-Stop-SANDAG-/b9mw-mf3n/about_data

[^3]: https://data.sandiego.gov/datasets/transit-stops/

---

## Questions

1. Over a year, can we calculate the frequency of trips along certain routes? How do we visualize that? 
1. Can we compare stops providing different levels of wheelchair accessibility? 
1. Can we compare different modes of transportation? Busses(Breeze/Contract/Direct) vs Rail(Trolley/Sprinter)?
1. How do ridership numbers change pre/post-COVID?
1. Can we drill down to individual routes and visualize them? Can we compare different routes to propose adding direct stops to other routes?  

^ I spent some time figuring out what the ask here, what is possible with the data that I have. 
^ Jobs to be done: framework → the more jobs a user can get done with a product, the more useful a product could be 

^ A job ex: Recommend new stops to existing stops going backward from that and see what info 

^ Flow charts, ordinal summary → violin plots, facets, networks, drop-down selections for years.

---

![fill](images/map.png)

^ Merging all of this data together lets me visualize all of the stops that are represented in the dataset. Even loading this up in Tableau for this view was just fantastic from a performance point of view : ( 

^ In this raw form this is hard to interpret
^ Let people select two different routes and compare them side by side. This will be shown as two lines connecting different stops
^ Flow map of these insights: the size of arrows to represent aggregated commuters traveling in different cardinal directions 
^ Filter across wheelchair accessibility, bus mode/ create facet views comparing those. 
^ Selector that let people visualize different years and facet across that

---

[.footer]
[.slidenumbers: false]
 
Vue/D3 starter[^4]:

^I am adamant in gaining more exposure into D3, I have a starter project I have found to get me going fast than starting from scratch. I cloned the repo and have pipelines building to this netlify URL  
^ Listed the starter repo here, shout out to margerite for responding to my emails and adding me on linked in :3
^ project to bootstrap
^ basic visualization build in already, 
^ controls/filters in UI, leading to answering these questions
^ biggest obstacle: Flow Maps 

![inline](images/mockup.png)

[^4]: https://github.com/margueriteroth/vue-d3-dataviz-starter

