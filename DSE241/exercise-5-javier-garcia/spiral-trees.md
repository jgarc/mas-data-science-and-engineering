footer: Javier Garcia

## Creating programmatic flow maps with spiral trees[^1]


![inline fill](images/minard.webp)

^ Minard's diagram of Napoleon Bonaparte's army trying to get to Russia. That cruel Russian winter. 


---

# What are flow maps? 


> Distributive flow maps are used to depict quantitative data. One or more sources are connected to several targets by lines whose thickness corresponds to the amount of flow between a source and a target.[^1]

* Typically drawn over a base map that exists underneath 
* Commonly seen as a hybrid between a Map and Sankey diagram
* Used to aggregate large quantities of data into a visual cue that communicates movement 

---
[.autoscale: true]

# What makes good flow maps? 


1. Good flow maps reduce visual clutter by merging (bundling) lines smoothly and by avoiding self-intersections.[^1] 


1. The widths of the flow lines of a flow tree are scaled proportionally (linearly) to the values they represent. When a flow line (trunk) separates into several smaller lines (branches) the width of the branches should add up to the width of the trunk[^1] (minimize lie factor) 

1. Flow maps often try to avoid covering important map features with flow trees to aid recognizability.[^1]

1. There is a tradeoff between the smoothness and frequency of merges[^1]


[^1]: Flow Map Layout via Spiral Trees, Kevin Verbeek, Kevin Buchin, and Bettina Speckmann

---

Timeline of programmatically generated flow maps

![inline](/Users/javi/projects/school/2022-jag043/DSE241/exercise-5-javier-garcia/images/history-of-automated-flow-maps.png)

---

> Our method is based on so-called spiral trees... Spiral trees naturally induce a clustering on the targets and smoothly bundle lines.[^1]

---
[.autoscale: true]

# Subset of Stenner tree which use logarithmic spirals


> all require an optimal interconnect for a given set of objects and a predefined objective function [^2]

* The Steiner Tree can help to reduce costs in many applications by finding the minimum cost tree that spans all the required nodes. [^3]

* Steiner Tree can improve the efficiency of networks and systems. [^3]

* While there are several approximation algorithms for the Steiner Tree Problem, they may not always provide optimal solutions. [^3]

* NP-hard, which means that finding the exact solution for large instances of the problem can be computationally infeasible. [^3]

[^2]: Steiner tree problem, Wikipedia

[^3]: Geeks for Geeks

---

![inline fill 60% left](images/spiral-trees-steps.png)

**root node**: the only node without a parent (red square).
**leaf node**: the only nodes without children (red).
**join node**: nodes where edges merge. One parent and at least two children (white).
**subdivision node**: used to avoid obstacles. One parent and one child (black).


---

To create trees with the smoothly merging lines of hand-drawn flow maps, α = 25deg , or α = 35deg in the presence of large obstacles


![inline left](images/steps to generate.png)

![inline fill left](images/spiral-trees-steps.png)

---

![inline](images/restriction angle tuning.png)

---

Global loss = Smoothing cost + Angle restriction cost + Balancing cost + Straightening cost

![inline](images/minimize-cost-with-gradient-descent.png)

---
[.footer: ]

![fit](images/georgia tech.png)

---
[.footer: ]

![fit](images/demo.png)

--- 

![left](images/demo.png)

![right](images/map.png)

---
# Apendix for repos with implementations for reference 

* https://github.com/glebpinigin/flowmapper

* https://ivi.cc.gatech.edu/spiral-tree/index.html

